package Lesson

fun main(args: Array<String>) {

    //Так пишутся комментарии

    /* так пишется много комментариев */

    println("div " + 5 / 5)

    println("Plus "+ (5 + 5))

    println("multiply " + 5 * 5)

    println("minus " + (5 - 5))

    println("div double " + (27.7 / 7.0))

    println("mod  " + (10 % 2))

    println("mod double " + 28.0 % 10.2)
    println("mod double and format " + "%.0f".format(28.0 % 10.2))
    //форматирование, вывести без цифр
    //после запятойё

    println()


    println("сдвиг влево " + (14 shl 2)) //смещаем два нуля в двоичной системе влево
    //было 00001110 стало 00111000

    println("сдвиг вправо " + (14 shr 2)) //смещаем два нуля в двоичной системе вправо
    //было 00001110 стало 00000011

}