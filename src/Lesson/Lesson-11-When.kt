package Lesson

fun main(args: Array<String>) {


    println("\n\nПростой When:")
    var number = 10
    when (number) {
        0 -> println("Zero")
        else -> println("Non-zero")
    }

    println("\n\nWhen с лямбдой:")
    number = 33
    println()
    when (number) {
        0 -> println("Zero")
        1 -> println("First")
        2 -> println("Second")
        3 -> {
            number += 10;
            println(number)
        }
        10 -> println("10")
        11, 33 -> println("11, 33")
        else -> println("Nothing")
    }


    number = 1

    val result = when (number) {
        1 -> "Ten"
        else -> "Unknown"
    }
    println(result)


    println("\n\nWhen сложный:")
    val hourOfDay = 12
    val timeOfDay: String
    timeOfDay = when (hourOfDay) {
        0, 1, 2, 3, 4, 5 -> "Early morning"
        6, 7, 8, 9, 10, 11 -> "Morning"
        12, 13, 14, 15, 16 -> "Afternoon"
        17, 18, 19 -> "Evening"
        20, 21, 22, 23 -> "Late evening"
        else -> "INVALID HOUR!"
    }
    println(timeOfDay)


    println("\n\nWhen использующий Ranges:")
    val timeOfDay2: String
    timeOfDay2 = when (hourOfDay) {
        in 0..5 step 2-> "Early morning"
        in 6..11 -> "Morning"
        in 12..16 -> "Afternoon"
        in 17..19 -> "Evening"
        in 20..23 -> "Late evening"
        in 20 until 30 -> "Test"
        in 40 downTo 50 -> "test2"
        else -> "INVALID HOUR!"
    }
    println(timeOfDay2)



    println("\n\nWhen в котором мат операции:")
    when {
        number % 2 == 0 -> println("Even")
        else -> println("Odd")
    }



    println("\n\nWhen с сложным условием:")
    val x = 1
    val y = 0
    val z = 0
    when {
        x == 0 && y == 0 && z == 0 -> println("Origin")
        y == 0 && z == 0 -> println("On the x-axis at x = $x")
        x == 0 && z == 0 -> println("On the y-axis at y = $y")
        x == 0 && y == 0 -> println("On the z-axis at z = $z")
        else -> println("Somewhere in space at x = $x, y = $y, z = $z")
    }




}