package Lesson

fun main(args: Array<String>) {

    var errorCode: Int? //в перемнной нул
    //println(errorCode) //error

    errorCode = null

    println("\n\nВывод переменной Int?:")
    var result: Int? = 30
    println(result)

    //println(result + 1)   //error

    var test = result
    //println(test + 1) ошибка

    var authorName: String? = "Dick Lucas"
    var authorAge: Int? = 24

    println("\n\nОперация над переменной Int?:")
    val ageAfterBirthday = authorAge!! + 1
    println("After their next birthday, author will be $ageAfterBirthday")

    authorAge = null
    //тут вылетит exception
    //println("After two birthdays, author will be ${authorAge!! + 2}")

    println("\n\nУсловие над переменной Int?:")
    if (authorAge != null) {
        authorAge = 1
    } else {
        authorAge = 2
    }
    println(authorAge)


    println("\n\nВывод Int?:")
    authorName = null
    val nameLength = authorName?.length
    println("Author's name has length $nameLength.")
    // > Author's name has length 10.

    authorName = "Dick Lucas"

    println("\n\nВызов функции plus() переменной Int?:")
    val nameLength2 = authorName?.length?.plus(5)
    println("Author's name has length $nameLength2.")

    println("\n\nВызов функции let() переменной Int?:")
    var nonNullableAuthor = ""
    authorName?.let {
        nonNullableAuthor = authorName
    }
    println(nonNullableAuthor)


    var nullableInt: Int? = 10
    var mustHaveResult = nullableInt ?: 0

    println("\n\nВывод результата после оператора nullableInt '?:' ")
    println(nullableInt)
    println(mustHaveResult)
    nullableInt = null

    println("\n\nВывод результата после оператора nullableInt '?:' ")
    println(nullableInt)
    println(mustHaveResult)
    mustHaveResult = nullableInt ?: 0   //оператор элвис, если nullableInt равен null
    //то мы загружаем ноль в mustHaveResult
    println(mustHaveResult)


    //тоже самое
    var nullableInt2: Int? = 10
    var mustHaveResult2 = if (nullableInt2 != null) nullableInt2 else 0



    var trst2123: Int? = 5
    println("\n\nВывод после операции над Int?")
    println(trst2123!! + 5)


}