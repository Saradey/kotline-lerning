package Lesson

fun main(args: Array<String>) {

    val evenNumber = arrayOf(2, 3, 4, 5) //Array <Int>.


    println("\n\nВывод обычного массива")
    println(evenNumber)
    println()

    // syntax in the chapter on lambdas.
    val fiveFives = Array(5, { 5 }) // 5, 5, 5, 5, 5


    val vowels = arrayOf("a", "e", "i", "o", "u")

    //Arrays of primitive types
    //При запуске Kotlin в JVM массив oddNumbers компилируется в массив Java
    val oddNumbers = intArrayOf(1, 3, 5, 7)

    val zeros = DoubleArray(4)

    val otherOddNumbers = arrayOf(1, 3, 5, 7).toIntArray()


    //Lists
    val innerPlanets = listOf("Mercury", "Venus", "Earth", "Mars")

    //ArrayLists
    val innerPlanetsArrayList = arrayListOf(
        "Mercury", "Venus", "Earth",
        "Mars"
    )

    val subscribers: List<String> = listOf()


    val subscribers2 = listOf<String>()

    val outerPlanets = mutableListOf(
        "Jupiter", "Saturn", "Uranus",
        "Neptune"
    )

    val exoPlanets = mutableListOf<String>()


    //Accessing elements
    val players = mutableListOf("Alice", "Bob", "Cindy", "Dan")
    print(players.isEmpty())

    println("\n\nПроверка на размер mutableList")
    if (players.size < 2) {
        println("We need at least two players!")
    } else {
        println("Let's start!")
    }
    // > Let's start!

    println("\n\nФункция mutableList first()")
    var currentPlayer = players.first()

    println(currentPlayer)

    println("\n\nФункция mutableList last()")
    println(players.last())


    println("\n\nФункция mutableList min()")
    val minPlayer = players.min()
    minPlayer.let {
        println("$minPlayer will start") // > Alice will start
    }


    println("\n\nФункция mutableList first() min()")
    println(arrayOf(2, 3, 1).first())
// > 2
    println(arrayOf(2, 3, 1).min())
// > 1

    println("\n\nФункция mutableList max()")
    val maxPlayer = players.max()
    if (maxPlayer != null) {
        println("$maxPlayer is the MAX") // > Dan is the MAX
    }

    println("\n\n[0] mutableList")
    val firstPlayer = players[0]
    println("First player is [0] $firstPlayer")
    // > First player is Alice

    val secondPlayer = players.get(1)

    //val player = players[4] // > IndexOutOfBoundsException


    //получение диапазона
    println("\n\nполучение диапазона функция slice() mutableList")
    val upcomingPlayersSlice = players.slice(0..3)
    println(upcomingPlayersSlice.joinToString()) // > Bob, Cindy


    println("\n\nпроверка на присутсвие через оператор in mutableList")
    println(isEliminated("Bob", players))
    println(isEliminated("Bobb", players))
    println(isEliminated2("Bob", players))

    println("\n\nфункция contains() mutableList")
    println(players.slice(0..3).contains("Alice"))



    players.add("Eli")
    players += "Gina"
    println("\n\nОперация += mutableList")
    println(players.joinToString())


    println("\n\nОперация += array")
    var array = arrayOf(1, 2, 3)
    array += 4
    println(array.joinToString())


    println("\n\nadd() index() - 2 mutableList")
    players.add(2, "Frank")
    println(players.joinToString())

    println("\n\nremove mutableList")
    val wasPlayerRemoved = players.remove("Gina")
    println("It is $wasPlayerRemoved that Gina was removed")
// > It is true that Gina was removed
    println(players.joinToString())

    println("\n\nremoveAt mutableList")
    val removedPlayer = players.removeAt(2)
    println("$removedPlayer was removed")
    println(players.joinToString())

    println("\n\nplayers[4] mutableList")
    players[4] = "Goncharov"
    println(players.joinToString())

    //сортировка
    println("\n\nsort() mutableList")
    players.sort()
    println(players.joinToString())

    println("\n\nset() mutableList")
    players.set(0, "Нуль")
    println(players.joinToString())

    println("\n\narrayOfInts[0] array")
    val arrayOfInts = arrayOf(1, 2, 3)
    arrayOfInts[0] = 4
    println(arrayOfInts.joinToString())



    var nullableList: List<Int>? = listOf(1, 2, 3, 4)
    nullableList = null

    var listOfNullables: List<Int?> = listOf(1, 2, null, 4)

    var listOfNullables2: List<Int?>? = listOf(null, null, null, null)


    //двух мерные массивы
    val table: Array<Array<Int>> = Array(3, { Array(5, {0}) })

}


fun isEliminated(player: String, players: MutableList<String>): Boolean {
    return player in players
}

fun isEliminated2(player: String, players: MutableList<String>): Boolean {
    return player !in players
}