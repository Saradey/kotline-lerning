package Lesson



fun main(args: Array<String>) {

    //размер фиксированный
    var yearOfBirth = mapOf("Anna" to 1990, "Brian" to 1991,
        "Craig" to 1992, "Donna" to 1993)


    println("\n\nmutableMap")
    var namesAndScores = mutableMapOf("Anna" to 2, "Brian" to 2, "Craig" to
            8, "Donna" to 6)  // > {Anna=2, Brian=2, Craig=8, Donna=6}
    println(namesAndScores)

    namesAndScores = mutableMapOf()

    var pairs = HashMap<String, Int>()

    pairs = HashMap<String, Int>(20)    //20 размер


    namesAndScores = mutableMapOf("Anna" to 2, "Brian" to 2, "Craig" to 8,
        "Donna" to 6)
    // Restore the values

    println("\n\nmutableMap []")
    println(namesAndScores["Anna"])

    println(namesAndScores["Greg"])

    println("\n\nmutableMap get()")
    println(namesAndScores.get("Craig"))

    println("\n\nmutableMap isEmpty()")
    println(namesAndScores.isEmpty())

    println("\n\nmutableMap size")
    println(namesAndScores.size)

    val bobData = mutableMapOf(
        "name" to "Bob",
        "profession" to "CardPlayer",
        "country" to "USA")

    bobData.put("state", "CA")

    println("\n\nmutableMap []=value")
    bobData["city"] = "San Francisco"

    println(bobData)

    //update
    println("\n\nmutableMap put")
    bobData.put("name", "Bobby") // Bob

    bobData["profession"] = "Mailman"

    println(bobData)

    println("\n\nmutableMap += pair")
    val pair = "nickname" to "Bobby"
    bobData += pair //добавляем в мап паир
    println(bobData)

    val pair2 = "name" to "Test"
    bobData += pair2
    println(bobData)

    println("\n\nmutableMap remove")
    bobData.remove("city")
    bobData.remove("state", "CAA")
    println(bobData)

    println("\n\nhashCode")
    println("some string".hashCode())
    println(1.hashCode())
    println(false.hashCode())

    println("\n\nSet:")
    //////////Set
    val names = setOf("Anna", "Brian", "Craig", "Anna")
    println(names)
    val hashSet = HashSet<Int>()

    val someArray = arrayOf(1, 2, 3, 1)

    println("\n\nSet создаем и загружаем array:")
    var someSet = mutableSetOf(*someArray)
    println(someSet)

    println("\n\nSet contains:")
    println(someSet.contains(1))

    println("\n\nSet in:")
    println(3 in someSet)

    println("\n\nSet add:")
    someSet.add(5)

    println("\n\nRemove:")
    val removedOne = someSet.remove(1)
    println(removedOne) // > true
    println(someSet)
    // > [2, 3, 5]

}
