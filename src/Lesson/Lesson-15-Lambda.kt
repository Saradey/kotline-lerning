package Lesson

fun main(args: Array<String>) {

    var multiplyLambda: (Int, Int) -> Int


    //multiplyLambda принимает два значения Int и возвращает Int.
    // Обратите внимание, что это точно так же, как объявление переменной
    // для функции.
    multiplyLambda = { a: Int, b: Int -> Int
        a * b
    }

    println("\n\nПростая Лямбда:")
    println(multiplyLambda(5, 5))

    multiplyLambda = { a, b ->
        a * b
    }


    println(multiplyLambda(5, 5))


    var doubleLambda = { a: Int ->
        2 * a
    }

    println("\n\ndoubleLambda:")
    println(doubleLambda(5))

    println("\n\ndoubleLambda it:")
    doubleLambda = { 2 * it }
    println(doubleLambda(4))

    println("\n\nsquare it*it:")
    val square: (Int) -> Int = { it * it }
    println(square(2))


    val myLambd = { a: Int, b: Int ->
        Int
        var aa = a
        aa += b
        aa * b
    }

    println("\n\nmyLambd:")
    operateOnNumbers(3, 3, myLambd)


    val addLambda = { a: Int, b: Int ->
        a + b
    }

    println("\n\nфункция которая принимает лямбды:")
    operateOnNumbers(4, 2, operation = addLambda)


    operateOnNumbers(4, 2, ::addFunction)

    operateOnNumbers(4, 2, { a: Int, b: Int ->
        a + b
    })

    operateOnNumbers(4, 2, operation = Int::plus)


    println("\n\nunitLambda:")
    var unitLambda: () -> Unit = {
        println("Kotlin Apprentice is awesome!")
    }
    unitLambda()

    println("\n\nnothingLambda:")
    var nothingLambda: () -> Nothing = { throw NullPointerException() }
    //nothingLambda()



    println("\n\nincrementCounter:")
    var counter = 0
    val incrementCounter = {
        counter += 1
    }
    incrementCounter()
    println(counter)


    println("\n\ncountingLambda()():")
    countingLambda()()
    countingLambda()()
    countingLambda()()



    val names = arrayOf("ZZZZZZ", "BB", "A", "CCCC", "EEEEE")
    names.sorted() // A, BB, CCCC, EEEEE, ZZZZZZ


    println("\n\nsortedWith List:")
    val namesByLength = names.sortedWith(compareBy {
        -it.length // > [ZZZZZZ, EEEEE, CCCC, BB, A]
    })
    println(namesByLength)


    val namesByLength2 = names.sortedWith(compareBy {
        +it.length // > A, BB, CCCC, EEEEE, ZZZZZZ
    })
    println(namesByLength2)


    println("\n\nforEach List:")
    val values = listOf(1, 2, 3, 4, 5, 6)
    values.forEach({
        println("$it: ${it * it}")
    })


    println("\n\nfilter List:")
    var prices = listOf(1.5, 10.0, 4.99, 2.30, 8.19)
    val largePrices = prices.filter({
        it > 5.0
    })
    println(largePrices.joinToString())


    println("\n\nmap List:")
    val salePrices = prices.map {
        it * 0.9
    }
    println(salePrices.joinToString())
    //[1.35, 9.0, 4.4910000000000005, 2.07, 7.3709999999999996]


    println("\n\nmap List toIntOrNull:")
    val userInput = listOf("0", "11", "haha", "42")
    val numbers = userInput.map {
        it.toIntOrNull()
    }
    println(numbers)

    println("\n\nmapNotNull List:")
    val numbers2 = userInput.mapNotNull {
        it.toIntOrNull()
    }
    println(numbers2)


    println("\n\nfold List:")
    //Начисляет значение, начинающееся с [начального] значения, и применяя
    // [операцию] слева направо к текущему значению аккумулятора
    // и каждому элементу.
    var sum = prices.fold(0.0) { a, b ->
        a + b
    }
    println(sum)



    var prices2 = listOf(1.5, 10.0)

    //Начисляет значение, начиная с первого элемента, и применяя [операцию]
    // слева направо к текущему значению аккумулятора и каждому элементу.
    println("\n\nreduce List:")
    sum = prices2.reduce { a, b ->
        a - b
    }
    println(sum)


    println("\n\nforEach map:")
    val stock = mapOf(1.5 to 5, 10.0 to 2, 4.99 to 20, 2.30 to 5, 8.19 to 30)
    var stockSum = 0.0
    stock.forEach {
        stockSum += it.key + it.value
    }
    println(stockSum)


}


fun operateOnNumbers(a: Int, b: Int, operation: (Int, Int) -> Int): Int {
    val result = operation(a, b)
    println(result)
    return result
}

fun addFunction(a: Int, b: Int) = a + b

fun addFunction2() = Unit


fun countingLambda(): () -> Int {
    var counter = 0
    val incrementCounter: () -> Int = {
        counter += 1
        println("countingLambda incrementCounter $counter")
        counter
    }
    return incrementCounter
}