package Lesson

fun main(args: Array<String>) {

    val person = Person("Sarad", "Appleseed")

    println("\n\nВывести результат переменной fullName:")
    println(person.fullName)

    val john = Person(firstName = "Johnny", lastName = "Appleseed")


    var var1 = Person(firstName = "Sarad", lastName = "Appleseed")
    var var2 = var1
    var1.firstName = "Test"
    println(var2.firstName)



    println("\n\nСравнение классов:")
    println(var1 == var2)
    println(var1 == person)



    var imposters = (0..100).map {
        Person(firstName = "John", lastName = "Appleseed")
    }


    imposters.map {
        it.firstName == "John" && it.lastName == "Appleseed"
    }.contains(true) // true

    println()


    val jane = Student(firstName = "Jane", lastName = "Appleseed", id = 1)
    val history = Grade(letter = "B", points = 9.0, credits = 3.0)
    var math = Grade(letter = "A", points = 16.0, credits = 4.0)
    jane.recordGrade(history)
    jane.recordGrade(math)


    println("\n\nВывести результат credits у jane:")
    println("${jane.credits}")


    val myJane = jane.copy()
    println("\n\nСравнения объектов:")
    println(myJane)
    println(myJane == jane) //equals
    println(myJane === jane)    //сравнение ссылок

    println("\n\nhashCode класса:")
    println(jane.hashCode())



    val marie = StudentData("Marie", "Curie", id = 1)
    val emmy = StudentData("Emmy", "Noether", id = 2)
    val marieCopy = marie.copy()
    println("\n\nвывод StudentData класса:")
    println(marie) // > StudentData(firstName=Marie, lastName=Curie, id=1)
    println(emmy)  // > StudentData(firstName=Emmy, lastName=Noether, id=2)
    println("\n\nСравнения объектов:")
    println(marie == emmy) // > false
    println(marie == marieCopy) // > true
    println(marie === marieCopy) // > false


    println("\n\nвывод StudentData переменных:")
    val (firstName, lastName, id) = marie
    println(firstName) // > Marie
    println(lastName)  // > Curie
    println(id)        // > 1



}


class Person(var firstName: String, var lastName: String) {
    val fullName
        get() = "$firstName $lastName"
}

class SimplePerson(val name: String)

class Grade(val letter: String, val points: Double, val credits: Double)


class Student(
    val firstName: String,
    var id: Int,
    val lastName: String,
    val grades: MutableList<Grade> = mutableListOf(),
    var credits: Double = 0.0
) {


    fun recordGrade(grade: Grade) {
        grades.add(grade)
        credits += grade.credits
    }


    override fun hashCode(): Int {
        val prime = 31
        var result = 1
        result = prime * result + firstName.hashCode()
        println("${firstName.hashCode()} firstName hashCode")
        result = prime * result + id
        result = prime * result + lastName.hashCode()
        println("${lastName.hashCode()} lastName hashCode")
        return result
    }


    override fun equals(other: Any?): Boolean {
        println("equals")
        if (this === other)
            return true
        if (other == null)
            return false
        if (javaClass != other.javaClass)
            return false
        val obj = other as Student?
        if (firstName != obj?.firstName)
            return false
        if (id != obj.id)
            return false
        if (lastName != obj.lastName)
            return false
        return true
    }

    override fun toString(): String {
        return "Student (firstName=$firstName, lastName=$lastName, id=$id)"
    }


    fun copy(
        firstName: String = this.firstName,
        lastName: String = this.lastName,
        id: Int = this.id
    ) = Student(firstName, id, lastName)


}

//в дата классе не нужно писать весь код с hash code, equals и тд
//он автоматически создается
data class StudentData(
    var firstName: String, var lastName: String, var
    id: Int
)
