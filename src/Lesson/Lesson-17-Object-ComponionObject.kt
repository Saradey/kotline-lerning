package Lesson


fun main(args: Array<String>) {

    println("\n\nвывод  статической x object:")
    println(X.x)


    val marie = Student2(1, "Marie", "Curie")
    val albert = Student2(2, "Albert", "Einstein")
    val richard = Student2(3, "Richard", "Feynman")

    StudentRegistry.addStudent(marie)
    StudentRegistry.addStudent(albert)
    StudentRegistry.addStudent(richard)
    println("\n\nвывод всех добавленных студентов из статического списка:")
    StudentRegistry.listAllStudents()
// > Curie, Marie
// > Einstein, Albert
// > Feynman, Richard

    println("\n\nстатический метод companion создающий класс Scientist:")
    var scientist = Scientist.newScientist("Test", "Test")
    println(scientist)


    println("\n\nстатический метод companion создающий класс Scientist:")
    val emmy = Scientist.Factory.newScientist("Emmy", "Noether")
    val isaac = Scientist.newScientist("Isaac", "Newton")
    val nick = Scientist.newScientist("Nikola", "Tesla")
    ScientistRepository.addScientist(emmy)
    ScientistRepository.addScientist(isaac)
    ScientistRepository.addScientist(nick)
    println("\n\nВывод из статического списка:")
    ScientistRepository.listAllScientists()


}


object X {
    var x = 0
}

data class Student2(
    val id: Int, val firstName: String, val lastName:
    String
) {
    var fullName = "$lastName, $firstName"
}

//хранилище студентов в видео синглетона
object StudentRegistry {
    val allStudents = mutableListOf<Student2>()
    fun addStudent(student: Student2) {
        allStudents.add(student)
    }

    fun removeStudent(student: Student2) {
        allStudents.remove(student)
    }

    fun listAllStudents() {
        allStudents.forEach {
            println(it.fullName)
        }
    }
}


object JsonKeys {
    const val JSON_KEY_ID = "id"
    const val JSON_KEY_FIRSTNAME = "first_name"
    const val JSON_KEY_LASTNAME = "last_name"
}


data class Scientist private constructor(
    val id: Int,
    val firstName: String,
    val lastName: String
) {

    //static
    companion object Factory{
        var currentId = 0
        fun newScientist(firstName: String, lastName: String): Scientist {
            currentId += 1
            return Scientist(currentId, firstName, lastName)
        }
    }


    var fullName = "$firstName $lastName"

    override fun toString(): String {
        return "Scientist(fullName='$fullName')"
    }
}


object ScientistRepository {
    val allScientists = mutableListOf<Scientist>()
    fun addScientist(student: Scientist) {
        allScientists.add(student)
    }

    fun removeScientist(student: Scientist) {
        allScientists.remove(student)
    }

    fun listAllScientists() {
        allScientists.forEach {
            println("${it.id}: ${it.fullName}")
        }
    }
}

