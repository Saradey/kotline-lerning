package Lesson

fun main(args: Array<String>) {


    //анонимный объект
    val counter = object : Counts {
        override fun studentCount(): Int {
            return 3
        }
        override fun scientistCount(): Int {
            return 3
        }
    }


    println(counter.studentCount()) // > 3
    println(counter.scientistCount()) // > 3

}


interface Counts {
    fun studentCount(): Int
    fun scientistCount(): Int
}