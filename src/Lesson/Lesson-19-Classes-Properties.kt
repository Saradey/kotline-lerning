package Lesson

import kotlin.math.roundToInt
import kotlin.properties.Delegates

//Properties
fun main(args: Array<String>) {

    println("\n\nСвойства:")
    var contact3 = Contact3(
        fullName = "Grace Murray", emailAddress = "grace@navy.mil"
    )

    val person = Person("Grace", "Hopper")
    person.fullName // Grace Hopper

    val address = Address()

    var tv: TV = TV(25.0, 25.0)
    println("\n\nВызова get() когда мы обращаемся к value:")
    println(tv.diagonal)    //только в этот момент вызовится


    tv.height = 30.0
    tv.width = 30.0
    println(tv.diagonal)    //только в этот момент вызовится

    println("\n\nВызова set() когда мы обращаемся к value:")
    tv.diagonal = 70
    println(tv.height) // 34.32...
    println(tv.width)


    val level1 = Level(id = 1, boss = "Chameleon", unlocked = true)
    val level2 = Level(id = 2, boss = "Squid", unlocked = false)
    val level3 = Level(id = 3, boss = "Chupacabra", unlocked = false)
    val level4 = Level(id = 4, boss = "Yeti", unlocked = false)

    //val highestLevel = level3.highestLevel error
    Level.highestLevel = 2
    Level.highestLevel2 = 2


    val delegatedlevel1 = DelegatedLevel(id = 1, boss = "Chameleon")
    val delegatedlevel2 = DelegatedLevel(id = 2, boss = "Squid")
    println("\n\nВывод companion object var:")
    println(DelegatedLevel.highestLevel) // 1

    println("\n\nDelegates.observable:")
    delegatedlevel2.unlocked = true
    println(DelegatedLevel.highestLevel) // 2


    println(delegatedlevel2.teamp)
    delegatedlevel2.teamp += 5
    println(delegatedlevel2.teamp)


    println("\n\nDelegates.vetoable:")
    println(delegatedlevel2.current)
    delegatedlevel2.current += 55
    println(delegatedlevel2.current)
    delegatedlevel2.current += 5
    println(delegatedlevel2.current)


    println("\n\nLazy properties:")
    val circle = Circle(5.0) // got a circle, pi has not been run
    val circumference = circle.circumference // 31.42
    println(circumference)
// also, pi now has a value


    println("\n\nlateinit у свойст не будет значений:")
    val lamp = Lamp()
// ... lamp has no lightbulb, need to buy some!
    //println(lamp.bulb)
// Error: kotlin.UninitializedPropertyAccessException:
// lateinit property bulb has not been initialized
// ... bought some new ones
    lamp.bulb = Circle(1.0)
    println(lamp.bulb)

    println("\n\nExtension properties :")
    println(lamp.bulb.diameter)
}

class Car(val make: String, val color: String)

class Contact(var fullName: String, var emailAddress: String)


class Contact3(
    var fullName: String,
    val emailAddress: String,
    //Default values
    var type: String = "Friend"
)


//Property initializers
class Person2(val firstName: String, val lastName: String) {
    val fullName = "$firstName $lastName"
}

class Address {
    var address1: String
    var address2: String? = null
    var city = ""
    var state: String

    //типо static блока
    init {
        address1 = ""
        state = ""
    }
}


class TV(var height: Double, var width: Double) {

    var diagonal: Int
        get() {

            println("get() TV")
            val result = Math.sqrt(height * height + width * width)
            return result.roundToInt()  //преоборазование
        }
        set(value) {
            val ratioWidth = 16.0
            val ratioHeight = 9.0
            val ratioDiagonal = Math.sqrt(
                ratioWidth * ratioWidth + ratioHeight *
                        ratioHeight
            )
            height = value.toDouble() * ratioHeight / ratioDiagonal
            width = height * ratioWidth / ratioHeight
        }
}


class Level(val id: Int, var boss: String, var unlocked: Boolean) {
    companion object {
        var highestLevel = 1

        @JvmStatic
        var highestLevel2 = 1
    }
}


class DelegatedLevel(val id: Int, var boss: String) {
    companion object {
        var highestLevel = 1
        const val maxCurrent = 40
    }

    //by
    //это лямбда с тремя аргументами, первый из которых - сам объект
    // свойства (который вы игнорируете), а второй и третий - старое и
    // новое значение свойства соответственно. Лямбда вызывается после
    // изменения значения, поэтому новый действительно имеет новое значение.
    var unlocked: Boolean by Delegates.observable(false) { _, old, new ->
        if (new && id > highestLevel) {
            highestLevel = id
        }
        println("$old -> $new")
    }

    //срабатывает при изменение переменной
    var teamp: Int by Delegates.observable(1) { observable, old, new ->
        println("x: $observable")
        println("old: $old")
        println("new: $new")
    }

    //ограничения, вызывается только в том случае если мы вернем true
    var current by Delegates.vetoable(0) { _, _, new ->
        if (new > maxCurrent) {
            println("Current too high, falling back to previous setting.")
            false
        } else {
            true
        }
    }

}


//Ленивые свойства
class Circle(var radius: Double = 0.0) {
    val pi: Double by lazy {
        println("lazy pi")
        ((4.0 * Math.atan(1.0 / 5.0)) - Math.atan(1.0 / 239.0)) * 4.0
    }
    val circumference: Double
        get() = pi * radius * 2
}


//у свойства не будет значения
class Lamp {
    lateinit var bulb: Circle
}


val Circle.diameter: Double
    get() = 2.0 * radius