package Lesson

fun main(args: Array<String>) {
    val months = arrayOf(
        "January", "February", "March",
        "April", "May", "June",
        "July", "August", "September",
        "October", "November", "December"
    )


    class SimpleDate1(var month: String)

    fun monthsUntilWinterBreak(from: SimpleDate1): Int {
        return months.indexOf("December") - months.indexOf(from.month)
    }


    class SimpleDate2(var month: String) {
        fun monthsUntilWinterBreak(from: SimpleDate2): Int {
            return months.indexOf("December") - months.indexOf(from.month)
        }
    }




    println("\n\nПример метода в классе:")
    val date2 = SimpleDate2("October")
    println(date2.monthsUntilWinterBreak(date2))


    println("\n\nМетод companion:")
    println(MyMath.factorial(6)) // 720)




    fun SimpleDate2.monthsUntilSummerBreak(): Int {
        val monthIndex = months.indexOf(month)
        return if (monthIndex in 0..months.indexOf("June")) {
            months.indexOf("June") - months.indexOf(month)
        } else if (monthIndex in
            months.indexOf("June")..months.indexOf("August")) {
            0
        } else {
            months.indexOf("June") + (12 - months.indexOf(month))
        }
    }

    println("\n\nМы изменили метод monthsUntilSummerBreak в классе SimpleDate2:")
    val date = SimpleDate2("October")
    date.month = "December"
    println(date.monthsUntilSummerBreak())


    println("\n\nМы изменили метод abs в классе Int:")
    fun Int.abs(): Int {
        return if (this < 0) -this else this
    }
    println(4.abs())    // > 4
    println((-4).abs()) // > 4




    fun MyMath.Companion.primeFactors(value: Int): List<Int> {
        // 1
        var remainingValue = value
        // 2
        var testFactor = 2
        val primes = mutableListOf<Int>()
        // 3
        while (testFactor * testFactor <= remainingValue) {
            if (remainingValue % testFactor == 0) {
                primes.add(testFactor)
                remainingValue /= testFactor
            } else {
                testFactor += 1
            }
        }
        if (remainingValue > 1) {
            primes.add(remainingValue)
        }
        return primes
    }

    println("\n\nМы добавили в Companion функцию primeFactors:")
    println(MyMath.primeFactors(81))


    println("\n\nПример Extensions методы:")
    fun MutableList<Int>.swap(index1: Int, index2: Int) {
        println("мы добавили функциональность MutableList функцию swap")
    }
    val lst = mutableListOf(1, 2, 3)
    lst.swap(0, 2)


}




class MyMath {
    // 1
    companion object {
        fun factorial(number: Int): Int {
// 2
            return (1..number).fold(1) { a, b -> a * b }
        }
    }
}


