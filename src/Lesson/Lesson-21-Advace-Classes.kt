package Lesson

fun main(args: Array<String>) {

    val john = Person3(firstName = "Johnny", lastName = "Appleseed")
    val jane = Student3(firstName = "Jane", lastName = "Appleseed")

    john.fullName() // Johnny Appleseed
    jane.fullName() // Jane Appleseed

    val history = Grade2(letter = 'B', points = 9.0, credits = 3.0)
    jane.recordGrade(history)
    // john.recordGrade(history) // john is not a student!

    println("\n\nВывод полного имени наследуемый метод")
    println(jane.fullName())


    val person = Student4(firstName = "Johnny", lastName = "Appleseed")
    val oboePlayer = OboePlayer(firstName = "Jane", lastName = "Appleseed")
    println("\n\nПолимофизм")
    println(phonebookName(person))     // Appleseed, Johnny
    println(phonebookName(oboePlayer)) // Appleseed, Jane


    var hallMonitor = Student4(firstName = "Jill", lastName = "Bananapeel")
    hallMonitor = oboePlayer

    println("\n\nоператор is типо instance of как в java")
    println(hallMonitor is OboePlayer)
    println(hallMonitor !is OboePlayer)
    println(hallMonitor is Person3)


    var myTest = BandMember(firstName = "Jill", lastName = "Bananapeel")
    println("\n\nпример использование оператора as")
    println(afterClassActivity(myTest))
    println(afterClassActivity(myTest as Student4))
    //родительский класс не может быть использован как as BandMember
    //выскочит ошибка


    println("\n\nпереопределеная функция recordGrade")
    val math = Grade2(letter = 'B', points = 9.0, credits = 3.0)
    val science = Grade2(letter = 'F', points = 9.0, credits = 3.0)
    val physics = Grade2(letter = 'F', points = 9.0, credits = 3.0)
    val chemistry = Grade2(letter = 'F', points = 9.0, credits = 3.0)
    val dom = StudentAthlete(firstName = "Dom", lastName = "Grady")
    dom.recordGrade(math)
    dom.recordGrade(science)
    dom.recordGrade(physics)
    println(dom.isEligible) // > true
    dom.recordGrade(chemistry)
    println(dom.isEligible) // > false

    println("\n\nТак как мы вызывали метод супер результат размер родительского списка")
    println(dom.grades.size)

    println("\n\nДва конструктора у одного класса")
    val shape2test = Shape2(5)
    //здесь вызывается первый и второй конструкторы
    val shape3test = Shape2(5, "Str")
    val shape4test = Shape2(1.1)


}


data class Grade2(
    val letter: Char, val points: Double, val credits:
    Double
)

//open класс открыт к наследованию
open class Person3(var firstName: String, var lastName: String) {
    fun fullName() = "$firstName $lastName"
}

class Student3(
    firstName: String, lastName: String,
    var grades: MutableList<Grade2> = mutableListOf<Grade2>()
) : Person3(firstName, lastName) {

    open fun recordGrade(grade: Grade2) {
        grades.add(grade)
    }

}


//Цепочка подклассов называется иерархией классов. В этом примере
// иерархия будет выглядеть как OboePlayer → BandMember → Student → Person.
// Иерархия классов аналогична семейному древу. Из-за этой аналогии суперкласс
// также называется родительским классом своего дочернего класса.
open class Student4(
    firstName: String, lastName: String,
    var grades: MutableList<Grade2> = mutableListOf<Grade2>()
) : Person3(firstName, lastName) {

    open fun recordGrade(grade: Grade2) {
        grades.add(grade)
    }

}


open class BandMember(firstName: String, lastName: String) :
    Student4(firstName, lastName) {
    open val minimumPracticeTime: Int
        get() {
            return 2
        }
}


class OboePlayer(firstName: String, lastName: String) :
    BandMember(firstName, lastName) {
    // This is an example of an override, which we’ll cover soon.
    override val minimumPracticeTime: Int = super.minimumPracticeTime * 2
}


fun phonebookName(person: Student4): String {
    return "${person.lastName}, ${person.firstName}"
}


//для оператора as
fun afterClassActivity(student: Student4): String {
    return "Student4!"
}

fun afterClassActivity(student: BandMember): String {
    return "BandMember!"
}


class StudentAthlete(firstName: String, lastName: String) :
    Student4(firstName, lastName) {

    val failedClasses = mutableListOf<Grade2>()

    override fun recordGrade(grade: Grade2) {
        super.recordGrade(grade)

        failedClasses.add(grade)
    }

    val isEligible: Boolean
        get() {
            return failedClasses.size > 3 //преоборазование
        }
}


class FinalStudent(firstName: String, lastName: String) :
    Person3(firstName, lastName)

//нужно open
class FinalStudentAthlete(firstName: String, lastName: String)
//: FinalStudent(firstName, lastName) // Build error!


open class AnotherStudent(firstName: String, lastName: String) : Person3(firstName, lastName) {

    open fun recordGrade(grade: Grade) {}

    //это касает и функций
    fun recordTardy() {}
}

class AnotherStudentAthlete(firstName: String, lastName: String) : AnotherStudent(firstName, lastName) {
    override fun recordGrade(grade: Grade) {} // OK

    //override fun recordTardy() {} // Build error! recordTardy is final
}


class Person4(var firstName: String, var lastName: String) {
    fun fullName() = "$firstName $lastName"
}

// is the same as
class Person5 constructor(var firstName: String, var lastName: String) {
    fun fullName() = "$firstName $lastName"
}

open class Shape2 {
    constructor(size: Int) {
        println("constructor first")
    }

    constructor(size: Int, color: String) : this(size) {
        println("constructor second")
    }

    constructor(tetsa: Double) {
        println("constructor 3")
    }
}

