package Lesson


fun main(args: Array<String>) {


    val human = Human("1/1/2000")
    //val mammal = Mammal("1/1/2000")

}


//open by default
abstract class Mammal(val birthDate: String) {
    abstract fun consumeFood()
}


class Human(birthDate: String) : Mammal(birthDate) {
    override fun consumeFood() {
// ...
    }

    fun createBirthCertificate() {
// ...
    }
}

