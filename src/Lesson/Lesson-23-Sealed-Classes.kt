package Lesson

fun main(args: Array<String>) {
    val circle1 = Shape.Circle(4)
    val circle2 = Shape.Circle(2)
    val square1 = Shape.Square(4)
    val square2 = Shape.Square(2)

    println(size(circle1))
    println(size(square2))

    println("\n\nсоздание sealed класса")
    val currency = AcceptedCurrency.Crypto()
    println("You've got some $currency!")

    println("\n\nПример с удобным использование sealed")
    println(currency.name)


    val currency2 = AcceptedCurrency2.Crypto()
    currency2.amount = .27541f
    println("${currency2.amount} of ${currency2.name} is "
            + "${currency2.totalValueInDollars()} in Dollars")

}

//похожи на enum-классы
sealed class Shape {
    class Circle(val radius: Int) : Shape()
    class Square(val sideLength: Int) : Shape()
}


fun size(shape: Shape): Int {
    return when (shape) {
        is Shape.Circle -> shape.radius
        is Shape.Square -> shape.sideLength
    }
}


sealed class AcceptedCurrency {
    class Dollar : AcceptedCurrency()
    class Euro : AcceptedCurrency()
    class Crypto : AcceptedCurrency()

    val name: String
        get() = when (this) {
            is Euro -> "Euro"
            is Dollar -> "Dollars"
            is Crypto -> "NerdCoin"
        }
}


sealed class AcceptedCurrency2 {
    abstract val valueInDollars: Float
    var amount: Float = 0.0f

    class Dollar : AcceptedCurrency2() {
        override val valueInDollars = 1.0f
    }

    class Euro : AcceptedCurrency2() {
        override val valueInDollars = 1.25f
    }

    class Crypto : AcceptedCurrency2() {
        override val valueInDollars = 2534.92f
    }
    // leave the existing name property alone

    fun totalValueInDollars(): Float {
        return amount * valueInDollars
    }


    val name: String
        get() = when (this) {
            is Euro -> "Euro"
            is Dollar -> "Dollars"
            is Crypto -> "NerdCoin"
        }
}