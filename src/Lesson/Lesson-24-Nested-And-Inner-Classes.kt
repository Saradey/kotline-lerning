package Lesson


fun main(args: Array<String>) {


    println("\n\nПример внутреннего класса inner")
    val mazda = Car2("mazda")
    val mazdaEngine = mazda.Engine("rotary")
    println(mazdaEngine)


}


//Nested
class Car3(val carName: String) {
    class Engine(val engineName: String) {
        override fun toString(): String {
            //return "$engineName in a $carName" // Error: cannot see outer
            //scope!
            return ""
        }
    }
}


class Car2(val carName: String) {


    inner class Engine(val engineName: String) {
        override fun toString(): String {
            return "$engineName engine in a $carName"
        }
    }

}


