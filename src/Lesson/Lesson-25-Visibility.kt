package Lesson

fun main(args: Array<String>) {

    println("\n\nПример модификаторов private public и др")
    val privilegedUser = PrivilegedUser(
        username = "sashinka", id = "1234",
        age = 21
    )
    val privilege = Privilege(1, "invisibility")
    privilegedUser.addPrivilege(privilege)
    println(privilegedUser.about()) // > sashinka, 21


    val test: User = User("name", "2", 2)
    //println(test.id)  потому что оно private


}

data class Privilege(val id: Int, val name: String)

open class User(
    val username: String,
    private val id: String,
    protected var age: Int
) {
    val firstName: String = "1"
    val lastName: String = "2"

}


class PrivilegedUser(username: String, id: String, age: Int) :
    User(username, id, age) {
    private val privileges = mutableListOf<Privilege>()

    fun addPrivilege(privilege: Privilege) {
        privileges.add(privilege)
    }

    fun hasPrivilege(id: Int): Boolean {
        return privileges.map { it.id }.contains(id)
    }

    fun about(): String {
        //return "$username, $id" // Error: id is private
        return "$username, $age" // OK: age is protected
    }
}


