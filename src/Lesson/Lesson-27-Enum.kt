package Lesson

import java.util.*


fun main(args: Array<String>) {

    println("\n\nВывод всех значений enum class DayOfTheWeek ")
    for (day in DayOfTheWeek.values()) {
        println("Day ${day.ordinal}: ${day.name}")
    }

    println("\n\nВывод values() по индексу")
    val dayIndex = 0
    val dayAtIndex = DayOfTheWeek.values()[dayIndex]
    println("Day at $dayIndex is $dayAtIndex")


    println("\n\nВывод valueOf")
    val tuesday = DayOfTheWeek.valueOf("Tuesday")
    println("Tuesday is day ${tuesday.ordinal}")

    //val notADay = DayOfTheWeek.valueOf("Blernsday")
    //println("Not a day: $notADay") error

    println("\n\nПример типов в enum")
    val day = DayOfTheWeek2.Monday
    println("Day ${day.ordinal}: ${day.name}, is weekend: ${day.isWeekend}")


    println("\n\nКомпоньон в enum вызов Calendar из стандартной библиотеки")
    val day2 = DayOfTheWeek3.today()
    println("Day ${day2.ordinal}: ${day2.name}, is weekend: ${day2.isWeekend}")


    println("\n\nФункции в enum")
    println(day2.daysUntil(day2))


    println("\n\nEnum и when")
    when (day2) {
        DayOfTheWeek3.Monday -> println("I don't care if $day2's blue")
        DayOfTheWeek3.Tuesday -> println("$day2's gray")
        DayOfTheWeek3.Wednesday -> println("And $day2, too")
        DayOfTheWeek3.Thursday -> println("$day2, I don't care 'bout you")
        DayOfTheWeek3.Friday -> println("It's $day2, I'm in love")
        DayOfTheWeek3.Saturday -> println("$day2, Wait...")
        DayOfTheWeek3.Sunday -> println("$day2 always comes too late")
    }


    println("\n\nNull и Enum")
    var muEnumVaribal: MyEnumClass? = MyEnumClass.One
    muEnumVaribal = null

    when (muEnumVaribal) {
        null -> println("Оно нулевое")
    }





}


enum class DayOfTheWeek {
    Sunday,
    Monday,
    Tuesday,
    Wednesday,
    Thursday,
    Friday,
    Saturday
}


enum class DayOfTheWeek2(val isWeekend: Boolean = false) {
    Monday(false),
    Tuesday(false),
    Wednesday(false),
    Thursday(false),
    Friday(false),
    Saturday(true),
    Sunday(true)
}


enum class DayOfTheWeek3(val isWeekend: Boolean = false) {
    Monday(false),
    Tuesday(false),
    Wednesday(false),
    Thursday(false),
    Friday(false),
    Saturday(true),
    Sunday(true);

    companion object {
        fun today(): DayOfTheWeek3 {
            // 1
            val calendarDayOfWeek = Calendar.getInstance().get(Calendar.DAY_OF_WEEK)
            // 2
            var adjustedDay = calendarDayOfWeek - 2
            // 3
            val days = DayOfTheWeek3.values()
            if (adjustedDay < 0) {
                adjustedDay += days.count()
            }
            // 4
            val today = days.first { it.ordinal == adjustedDay }
            return today
        }

    }


    fun daysUntil(other: DayOfTheWeek3): Int {
        if (this.ordinal < other.ordinal) {
            return other.ordinal - this.ordinal
        } else {
            return other.ordinal - this.ordinal +
                    DayOfTheWeek.values().count()
        }
    }
}


enum class MyEnumClass{
    One,
    Two,
    three,
    four,
    five,
    sex,
    seven
}