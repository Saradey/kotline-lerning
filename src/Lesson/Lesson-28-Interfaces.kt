package Lesson


fun main(args: Array<String>) {

    println("\n\nДемонстрация интерфейса")
    val car = OptionalDirection()

    println("\n\nМетод интерфейса установлен значение по умолчанию")
    car.turn() // > LEFT
    println("\n\nПередаем значение в метод")
    car.turn(Direction.RIGHT)


    println("\n\nДефотный метод в интерфейсе")
    val falcon = LightFreighter()
    falcon.accelerate()
    falcon.stop()

    println("\n\nДефотный метод в интерфейсе LightFreighter2")
    val falcon2 = LightFreighter2()
    println(afterClassActivity2(falcon2 as SpaceVehicle))


    println("\n\nПример интерфейсов в качестве итераторов в циклах")
    val cars = listOf("Lamborghini", "Ferrari", "Rolls-Royce")
    val numbers = mapOf("Brady" to 12, "Manning" to 18, "Brees" to 9)
    for (car in cars) {
        println(car)
    }
    for (qb in numbers) {
        println("${qb.key} wears ${qb.value}")
    }


    println("\n\nПример множественного наследования интерфейсов")
    val titanic = Boat()
    titanic.length = 883
    val qe2 = Boat()
    qe2.length = 963
    println(titanic > qe2) // > false


}

interface Strike {
    fun strike()
    fun addAmmo()
}

class Archer : Strike {

    var ammo: Int = 0

    override fun strike() {
        if (ammo != 0)
            ammo--
    }

    override fun addAmmo() {
        if (ammo < 30)
            ammo++
    }
}

enum class Direction {
    LEFT, RIGHT
}

interface DirectionalVehicle {
    fun accelerate()
    fun stop()
    fun turn(direction: Direction)
    fun description(): String
}

//дефолтное значение переданное в метод
interface OptionalDirectionalVehicle {
    fun turn(direction: Direction = Direction.LEFT)
}

class OptionalDirection : OptionalDirectionalVehicle {
    override fun turn(direction: Direction) {
        println(direction)
    }
}

//дефолтный метод
interface SpaceVehicle {
    fun accelerate()
    fun stop() {
        println("SpaceVehicle")
    }

}

class LightFreighter : SpaceVehicle {
    override fun accelerate() {
        println("LightFreighter")
    }
}

class LightFreighter2 : SpaceVehicle {
    override fun accelerate() {

    }

    override fun stop() {
        println("LightFreighter2")
    }

}

fun afterClassActivity2(space: LightFreighter2): String {
    return "LightFreighter2!"
}

fun afterClassActivity2(space: SpaceVehicle): String {
    return "SpaceVehicle!"
}


//Properties in interfaces
interface VehicleProperties {
    val weight: Int // abstract
    val name: String
        get() = "Vehicle"
}

class Car4 : VehicleProperties {
    override val weight: Int = 1000
}

class Tank : VehicleProperties {
    override val weight: Int
        get() = 10000
    override val name: String
        get() = "Tank"
}

//интерфейс наследует от интерфейса
interface WheeledVehicle : SpaceVehicle {
    val numberOfWheels: Int
    var wheelSize: Double
}


class Bike : WheeledVehicle {
    var peddling = false
    var brakesApplied = false
    override val numberOfWheels = 2
    override var wheelSize = 622.0
    override fun accelerate() {
        peddling = true
        brakesApplied = false
    }

    override fun stop() {
        peddling = false
        brakesApplied = true
    }
}


//Implementing multiple interfaces

interface Wheeled {
    val numberOfWheels: Int
}

class Tricycle() : Wheeled, WheeledVehicle {

    override val numberOfWheels: Int
        get() = 1

    override var wheelSize: Double = 1.1
        get() = 1.2


    override fun accelerate() {

    }
}


public interface Comparable<in T> {
    public operator fun compareTo(other: T): Int
}

interface SizedVehicle {
    var length: Int
}

class Boat : SizedVehicle, Comparable<Boat> {
    override var length: Int = 0
    override fun compareTo(other: Boat): Int {
        return when {
            length > other.length -> 1
            length == other.length -> 0
            else -> -1
        }
    }
}