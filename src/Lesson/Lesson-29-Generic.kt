package Lesson

fun main(args: Array<String>) {

    println("Пример дженерика с Any")
    val things = mutableListOf<Any>(1, 2)
    things.add("SSSSS")
    println(things.joinToString())


    println("\n\nПример дженерика выводим по ключу")
    val map = mapOf(
        Pair("one", 1),
        Pair("two", "II"),
        Pair("three", 3.0f)
    )

    val valuesForKeysWithE = map.keys
        .filter { it.contains("e") }
        .map { "Value for $it: ${map[it]}" }

    println("Values for keys with E: $valuesForKeysWithE")


    println("Пример, как можно добавлять функционал коллекциям")
    //добавили функцию к листу
    fun List<String>.toBulletedList(): String {
        val separator = "\n - "
        return this.map { "$it" }.joinToString(
            separator, prefix = separator,
            postfix = "\n"
        )
    }

    val names = listOf("Jon", "Doom", "Test")
    println("Names: ${names.toBulletedList()}")

    println("Values for keys with E: ${valuesForKeysWithE.toBulletedList()}")


    //println("Things: ${things.toBulletedList()}") error


    //дорабатываем
    fun <T> List<T>.toBulletedList2(): String {
        val separator = "\n - "
        return this.map { "$it" }.joinToString(
            separator, prefix = separator,
            postfix = "\n"
        )
    }



    println("\n\nПример работы моего дженерик класса")
    val television = BreakableThing("Flat-Screen Television")
    val breakableThings = listOf(
        television,
        BreakableThing("Mirror"),
        BreakableThing("Guitar")
    )
    val expensiveMover = Mover(breakableThings)

    expensiveMover.moveEverythingToTruck()
    expensiveMover.moveEverythingIntoNewPlace()
    expensiveMover.finishMove()


    println("\n\nПроекция звезды")
    val ar = arrayOf(1, 2, 3)
    tryToMoveItemIntoNewPlace(ar)


    val ints = listOf(1, 2, 3)
    val numbers: List<Number> = ints
    //val moreInts: List<Int> = numbers error


    val mutableInts = mutableListOf(1, 2, 3)
    //val mutableNumbers: MutableList<Number> = mutableInts error



}


interface List2<out E> : Collection<E>


class Mover<T : Checkable>(
    // 2
    thingsToMove: List<T>,
    val truckHeightInInches: Int = (12 * 12)
) {

    private var thingsLeftInOldPlace = mutableListOf<T>()
    private var thingsInTruck = mutableListOf<T>()
    private var thingsInNewPlace = mutableListOf<T>()


    init {
        thingsLeftInOldPlace.addAll(thingsToMove)
    }


    fun moveEverythingToTruck() {
        while (thingsLeftInOldPlace.count() > 0) {
            val item = thingsLeftInOldPlace.removeAt(0)
            thingsInTruck.add(item)
            println("Moved your $item to the truck!")
        }
    }


    fun moveEverythingIntoNewPlace() {
        while (thingsInTruck.count() > 0) {
            val item = thingsInTruck.removeAt(0)
            thingsInNewPlace.add(item)
            println("Moved your $item into your new place!")
        }
    }


    fun finishMove() {
        println("OK, we're done! We were able to move your:")
    }


}


class BreakableThing(
    val name: String,
    var isBroken: Boolean = false
) : Checkable {
    fun smash() {
        isBroken = true
    }

    override fun toString(): String {
        return name
    }

    override fun checkIsOK(): Boolean {
        return true
    }
}


interface Checkable {
    fun checkIsOK(): Boolean
}

interface Container<T> {
    // 2
    fun canAddAnotherItem(): Boolean

    fun addItem(item: T)
    // 3
    fun canRemoveAnotherItem(): Boolean

    fun removeItem(): T
    // 4
    fun getAnother(): Container<T>

    // 5
    fun contents(): List<T>


}


fun <T> moveContainerToTruck(container: Container<T>) {

}

fun <T> tryToMoveItemIntoNewPlace(item: T) {

    if (item is T) {

    }

    if (item is Array<*>) {
        println("Мы передали Array")
    }
}



class CardboardBox : Container<BreakableThing> {
    //2
    private var items = mutableListOf<BreakableThing>()

    override fun contents(): List<BreakableThing> {

        // 3
        return items.toList()
    }

    // 4
    override fun canAddAnotherItem(): Boolean {
        return items.count() < 2
    }

    override fun addItem(item: BreakableThing) {
        // 5
        items.add(item)
    }

    override fun canRemoveAnotherItem(): Boolean {
        // 6
        return items.count() > 0
    }

    override fun removeItem(): BreakableThing {
        // 7
        val lastItem = items.last()
        items.remove(lastItem)
        return lastItem
    }

    override fun getAnother(): Container<BreakableThing> {
        // 8
        return CardboardBox()
    }
}


interface Container2<out T> {
    // 2
    fun canAddAnotherItem(): Boolean

    //fun addItem(item: T)
    // 3
    fun canRemoveAnotherItem(): Boolean

    fun removeItem(): T
    // 4
    fun getAnother(): Container<out T>

    // 5
    fun contents(): List<T>

}

interface Container3<in T> {
    // 2
    fun canAddAnotherItem(): Boolean

    fun addItem(item: T)
    // 3
    fun canRemoveAnotherItem(): Boolean

    //fun removeItem(): T
    // 4
    //fun getAnother(): Container<out T>

    // 5
    //fun contents(): List<T>

}

