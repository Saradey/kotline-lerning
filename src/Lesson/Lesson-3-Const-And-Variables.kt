package Lesson

fun main(args: Array<String>) {


    val number: Int = 10 //константа типа int

    println(number)

    println(number - 1)

    var number2: Int = 10//обычная переменная

    //number -= 5; error

    number2 -= 5;

    println(number2)

    //const val reallyConstant: Int = 42 помечает переменную как константную
    //во время компиляции


    var variableNumber: Int = 42

    var variableNumber2 = 42;

    variableNumber = 1_000_000

    println(variableNumber)


    var counter: Int = 0
    counter += 1
    // counter = 1
    counter -= 1
    // counter = 0


    var count1 = 1
    var count2: Int = 2;
    var count3: Int = 3;
    var count4: Int = 4;
    var count5: Int = 5;
    var count6: Int = 6;
    var count7: Int = 7;
    var count8: Int = 8;
    var count9: Int = 9;
    var count10:Int = 10;

    counter *= 3  // same as counter = counter * 3
    // counter = 30
    counter /= 2  // same as counter = counter / 2
    // counter = 15

    var count = 1.0

    val wantADouble = 3.0


}