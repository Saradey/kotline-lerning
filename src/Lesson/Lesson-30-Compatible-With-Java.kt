package Lesson

import myjava.Lesson30UserJava


fun main(args: Array<String>) {

    val test = Lesson30UserJava()

    println("\n\nСоздали класс java выводим переменную на экран")
    println(test.test)



    val userTest = Lesson30UserJava()
}

data class Address2(
    val streetLine1: String,
    val streetLine2: String?,
    val city: String,
    val stateOrProvince: String,
    val postalCode: String,
    val country: String = "United States"
){

    fun forPostalLabel(): String {
        var printedAddress = streetLine1
        streetLine2?.let { printedAddress += "\n$streetLine2" }
        printedAddress += "\n$city, $stateOrProvince $postalCode"
        printedAddress += "\n${country.toUpperCase()}"
        return printedAddress
    }

}

class Address3{
    val fullName: String?
        get() = "Address3 " + " " + "Address3"

    private val two: String = ""

}


class UserExtensions{
    val fullName: String
        get() = "1" + " " + "2"
}

class Address4(val streetLine1: String)

data class Address5 @JvmOverloads constructor (val streetLine1: String)


//JvmField не генерирует сетеры и гетеры
//const тоже самое
object JSONKeys {
    @JvmField  val streetLine1 = "street_1"
    const val streetLine2 = "street_2"
    @JvmField  val city = "city"
    @JvmField  val stateOrProvince = "state"
    @JvmField  val postalCode = "zip"
    @JvmField  val addressType = "type"
    @JvmField  val country = "country"

}

//"Static" values and functions from
//Kotlin

class StaticClass {

    companion object {
        val sampleFirstLine = "123 Fake Street"

        const val sampleFirstLine2 = "123 Fake Street"

        fun staticFunFun() : String{
            return "staticFunFun"
        }

        @JvmStatic
        fun staticFunFun2() : String{
            return "staticFunFun2"
        }

    }

}