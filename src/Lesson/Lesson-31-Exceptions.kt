package Lesson

import java.util.*

fun main(args: Array<String>) {

    val spaceCraft = SpaceCraft()
    //SpacePort.investigateSpace(spaceCraft) вылезит Exception


    println("\n\nМы обрабатываем Exception в блоке try/catch")
    investigateSpace(spaceCraft)

    //spaceCraft.launch2() вылезит Exception

    println("\n\nМы обрабатываем Exception в блоке try/catch свой кастомный эксепшен")
    investigateSpace2(spaceCraft)


    println("\n\nэксепшен с блоком finally")
    investigateSpace3(spaceCraft)


    //эксепшены для переменных
    val date: Date = try {
        Date("11.11.11") // try to parse user input
    } catch (exception: IllegalArgumentException) {
        Date() // otherwise use current date
    }



}




class SpaceCraft {
    var isConnectionAvailable: Boolean = false
    var isEngineInOrder: Boolean = false
    var isInSpace: Boolean = false
    var fuel: Int = 0


    fun launch() {
        if (fuel < 5) {
            throw Exception("Out of fuel. Can't take off.")
        }

        if (!isEngineInOrder) {
            throw Exception("The engine is broken. Can't take off.")
        }

        if (!isConnectionAvailable) {
            throw Exception("No connection with Earth. Can't take off.")
        }

        sendMessageToEarth("Trying to launch...")

        fuel -= 5

        sendMessageToEarth("I'm in space!")
        sendMessageToEarth("I've found some extraterrestrials")
        isInSpace = true
    }


    fun launch2() {
        if (fuel < 5) {
            throw OutOfFuelException()
        }

        if (!isEngineInOrder) {
            throw BrokenEngineException()
        }

        if (!isConnectionAvailable) {
            throw SpaceToEarthConnectionFailedException()
        }

        sendMessageToEarth("Trying to launch...")

        fuel -= 5

        sendMessageToEarth("I'm in space!")
        sendMessageToEarth("I've found some extraterrestrials")
        isInSpace = true
    }

    fun refuel() {
        fuel += 5
        sendMessageToEarth("The fuel tank is filled.")
    }

    fun repairEngine() {
        isEngineInOrder = true
        sendMessageToEarth("The engine is in order.")
    }

    fun fixConnection() {
        isConnectionAvailable = true
        sendMessageToEarth("Hello Earth! Can you hear me?")
        sendMessageToEarth("Connection is established.")
    }

    fun land() {
        sendMessageToEarth("Landing...")
        isInSpace = false
    }

    fun sendMessageToEarth(message: String) {
        println("Spacecraft to Earth: $message")
    }
}


object SpacePort {

    fun investigateSpace(spaceCraft: SpaceCraft) {
        spaceCraft.launch()
    }

}


fun investigateSpace(spaceCraft: SpaceCraft) {
    try {
        spaceCraft.launch()
    } catch (exception: Exception) {
        spaceCraft.sendMessageToEarth(exception.localizedMessage)
    }
}


//Creating custom exceptions
class OutOfFuelException :
    Exception("Out of fuel. Can't take off.")

class BrokenEngineException :
    Exception("The engine is broken. Can't take off.")

class SpaceToEarthConnectionFailedException :
    Exception("No connection with Earth. Can't take off.")


fun investigateSpace2(spaceCraft: SpaceCraft) {
    try {
        spaceCraft.launch2()
    } catch (exception: OutOfFuelException) {
        spaceCraft.sendMessageToEarth(exception.localizedMessage)
    } catch (exception: BrokenEngineException) {
        spaceCraft.sendMessageToEarth(exception.localizedMessage)
    } catch (exception: SpaceToEarthConnectionFailedException) {
        spaceCraft.sendMessageToEarth(exception.localizedMessage)
    }
}


fun investigateSpace3(spaceCraft: SpaceCraft) {
    try {
        spaceCraft.launch2()
    } catch (exception: OutOfFuelException) {
        spaceCraft.sendMessageToEarth(exception.localizedMessage)
        spaceCraft.refuel()
    } catch (exception: BrokenEngineException) {
        spaceCraft.sendMessageToEarth(exception.localizedMessage)
        spaceCraft.repairEngine()
    } catch (exception: SpaceToEarthConnectionFailedException) {
        spaceCraft.sendMessageToEarth(exception.localizedMessage)
        spaceCraft.fixConnection()
    } finally {
        println("finally")
        if (spaceCraft.isInSpace) {
            spaceCraft.land()
        } else {
            investigateSpace(spaceCraft)
        }
    }
}

