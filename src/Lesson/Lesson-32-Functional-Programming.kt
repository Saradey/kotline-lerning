package Lesson

import Lesson.Battlefield.battle
import java.util.*


fun main(args: Array<String>) {
    val firstRobot = Robot("Experimental Space Navigation Droid")
    val secondRobot = Robot("Extra-Terrestrial Air Safety Droid")
    //Battlefield.beginBattle(firstRobot, secondRobot)

    println("\n\nпример работы функционального программирования")
    val int = someFunction()
    println(int())

    println("\n\nЛямбды в контексте функционального программирования")
    val pow = { base: Int, exponent: Int ->
        Math.pow(
            base.toDouble(),
            exponent.toDouble()
        )
    }

    println(pow(2, 4))

    val pow2: (Int, Int) -> Double = { base, exponent ->
        Math.pow(
            base.toDouble(),
            exponent.toDouble()
        )
    }

    val root: (Int) -> Double = { Math.sqrt(it.toDouble()) }

    println("\n\nпередаем в функцию лямбду")
    val onBattleEnded = { winner: Robot -> winner.report("Won!") }
    Battlefield.beginBattle(firstRobot, secondRobot, onBattleEnded)


    println("\n\nExtension functions")
    //Extension functions
    fun String.print() = System.out.println(this)

    val string = "Hello world"
    string.print()


    //Anonymous functions
    fun(robot: Robot) {
        robot.report("Won!")
    }

    val reportOnWin = fun(robot: Robot) { robot.report("Won!") }


    //Returning from lambdas
    println("\n\nReturning from lambdas")
    calculateEven3()


}

//Returning from lambdas
fun calculateEven() {
    var result = 0
    (0..20).forEach {
        if (it % 3 == 0) return
        if (it % 2 == 0) result += it
    }
    println(result)
}

fun calculateEven2() {
    var result = 0
    (0..20).forEach {
        if (it % 3 == 0) {
            println("Сработал первый $it")
            return@forEach
        }
        if (it % 2 == 0) {
            result += it
            println("Сработал второй $it")
        }
    }
    println("\n$result")
}

fun calculateEven3() {
    var result = 0
    (0..20).forEach loop@{
        if (it % 3 == 0) return@loop
        if (it % 2 == 0) result += it
    }
    println(result)
}

fun calculateEven4() {
    var result = 0
    (0..20).forEach(fun(value) {
        if (value % 3 == 0) return
        if (value % 2 == 0) result += value
    })
    println(result)
}

//end Returning from lambdas


fun someFunction(): () -> Int {
    return ::anotherFunction
}

fun anotherFunction(): Int {
    return Random().nextInt()
}


class Robot(val name: String) {
    var strength: Int = 0
    private var health: Int = 100
    var isAlive: Boolean = true

    init {
        strength = Random().nextInt(100) + 10
        report("Created (strength $strength)")
    }

    fun report(message: String) {
        println("$name: \t$message")
    }

    // 2

    fun attack(robot: Robot) {
        // 3
        val damage = (strength * 0.1 + Random().nextInt(10)).toInt()
// 4
        robot.damage(damage.toInt())
    }


    fun damage(damage: Int) {
        // 5
        val blocked = Random().nextBoolean()
        if (blocked) {
            report("Blocked attack")
            return
        }
// 6
        health -= damage
        report("Damage -$damage, health $health")
// 7
        if (health <= 0) {
            isAlive = false
        }
    }


    //infix
    infix fun attack2(robot: Robot) {

    }
}


object Battlefield {
    // 1
    fun beginBattle(firstRobot: Robot, secondRobot: Robot) {
        // 2
        var winner: Robot? = null
        // 3
        battle(firstRobot, secondRobot)
        // 4
        winner = if (firstRobot.isAlive) firstRobot else secondRobot
    }

    fun battle(firstRobot: Robot, secondRobot: Robot) {
        // 5
        firstRobot.attack(secondRobot)
// 6
        if (secondRobot.isAlive.not()) {
            return
        }
// 7
        secondRobot.attack(firstRobot)
        if (firstRobot.isAlive.not()) {
            return
        }
// 8
        battle(firstRobot, secondRobot)
    }

    fun beginBattle(
        firstRobot: Robot, secondRobot: Robot,
        onBattleEnded: (Robot) -> Unit
    ) {
        var winner: Robot? = null
        battle(firstRobot, secondRobot)
        winner = if (firstRobot.isAlive) firstRobot else secondRobot
        onBattleEnded(winner)
    }
}

//Inline functions
inline fun beginBattle(
    firstRobot: Robot, secondRobot: Robot,
    onBattleEnded: Robot.() -> Unit
) {


}

//noinline
inline fun someFunction(
    inlinedLambda: () -> Unit,
    noinline nonInlinedLambda: () -> Unit
) {

}


//crossinline
inline fun someFunction(crossinline body: () -> Unit) {
    //yetAnotherFunction {
    body()
    //}
}

//tailrec для рекурсии
tailrec fun battle(firstRobot: Robot, secondRobot: Robot) {

}


