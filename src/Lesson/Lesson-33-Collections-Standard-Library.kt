package Lesson

fun main(args: Array<String>) {
    val participants = arrayListOf<Robot>(
        Robot("Extra-Terrestrial Neutralization Bot"),
        Robot("Generic Evasion Droid"),
        Robot("Self-Reliant War Management Device"),
        Robot("Advanced Nullification Android"),
        Robot("Rational Network Defense Droid"),
        Robot("Motorized Shepherd Cyborg"),
        Robot("Reactive Algorithm Entity"),
        Robot("Ultimate Safety Guard Golem"),
        Robot("Nuclear Processor Machine"),
        Robot("Preliminary Space Navigation Machine")
    )

    //val topCategory = participants.filter { it.strength > 80 }

    val topCategory2 = participants
        // 1
        .filter { it.strength > 80 }
        // 2
        .take(3)
        // 3
        .sortedBy { it.name }

    //1. Filter robots by their strength to find the strongest ones.
    //2. Take only first the three robots.
    //3. Sort the remaining robots by their names alphabetically.

    println("\n\nВывести после ряда функци коллекции")
    println(topCategory2.joinToString())

}