package Lesson

import java.util.*

fun main(args: Array<String>) {
    println("\n\nSequences")
    //Sequences
    val random = Random()
// 1
    val sequence = generateSequence {
        // 2
        random.nextInt(100)
    }

    sequence.take(5)
        .sorted()
        .forEach { println(it) }


    println("\n\ngenerateSequence")
    val factorial = generateSequence(1 to 1) {
        it.first + 1 to it.second * (it.first + 1)
    }
    println(factorial.take(5).map { it.second }.last())


}