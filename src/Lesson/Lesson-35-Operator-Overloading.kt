package Lesson

import kotlin.reflect.KProperty

fun main(args: Array<String>) {


    // your company
    val company = Company("MyOwnCompany")
    // departments
    val developmentDepartment = Department("Development")
    val qaDepartment = Department("Quality Assurance")
    val hrDepartment = Department("Human Resources")
// employees
    var Julia = Employee(company, "Julia", 100_000)
    var John = Employee(company, "John", 86_000)
    var Peter = Employee(company, "Peter", 100_000)
    var Sandra = Employee(company, "Sandra", 75_000)
    var Thomas = Employee(company, "Thomas", 73_000)
    var Alice = Employee(company, "Alice", 70_000)
    var Bernadette = Employee(company, "Bernadette", 66_000)
    var Mark = Employee(company, "Mark", 66_000)

    println("\n\nинкремент в классе ")
    ++Julia
    Julia = Julia.inc();
    println(Julia.salary)

    println("\n\nминус в классе ")
    Julia -= 100000

    println("\n\nплюс в классе ")
    Julia += 20000

    println("\n\nтоже самое в классе ")
    Julia.plusAssign(2500);
    Julia.minusAssign(2000);

    println("\n\nплюс в классе ")
    company += developmentDepartment
    company += qaDepartment
    company += hrDepartment

    println("\n\nплюс в классе ")
    developmentDepartment += Julia
    developmentDepartment += John
    developmentDepartment += Peter
    qaDepartment += Sandra
    qaDepartment += Thomas
    qaDepartment += Alice
    hrDepartment += Bernadette
    hrDepartment += Mark

    println("\n\nминус в классе ")
    qaDepartment -= Thomas


    println("\n\nget [] в классе ")
    val firstEmployee = qaDepartment[0]
    println(firstEmployee)

    qaDepartment[0]!! += 1000000
    println(qaDepartment[0]!!.salary)


    println("\n\nset [] в классе ")
    qaDepartment[1] = Thomas


    println("\n\nin в классе ")
    if (Thomas in qaDepartment) {
        println("${Thomas.name} no longer works here")
    }

    println("\n\nforEach в моем классе ")
    developmentDepartment.forEach {
        println("forEach developmentDepartment")
    }


    println("\n\nиз двух классов мы сделали list ")
    //print((Alice..Mark).joinToString { it.name })



    var name: String by NameDelegate()

}


class Company(val name: String) {
    val departments: ArrayList<Department> = arrayListOf()

    val allEmployees: List<Employee>
        get() = arrayListOf<Employee>().apply {
            departments.forEach { addAll(it.employees) }
        }


    operator fun plusAssign(department: Department) {
        departments.add(department)
    }

    operator fun minusAssign(department: Department) {
        departments.remove(department)
    }
}

//Iterable дает функцию for_each
class Department(val name: String) : Iterable<Employee> {
    val employees: ArrayList<Employee> = arrayListOf()

    operator fun plusAssign(employee: Employee) {
        employees.add(employee)
        println("${employee.name} hired to $name department")
    }


    operator fun minusAssign(employee: Employee) {
        if (employees.contains(employee)) {
            employees.remove(employee)
            println("${employee.name} fired from $name department")
        }
    }


    operator fun get(index: Int): Employee? {
        return if (index < employees.size) {
            employees[index]
        } else {
            null
        }
    }


    operator fun set(index: Int, employee: Employee) {
        println("set $index")
        if (index < employees.size) {
            employees[index] = employee
        }
    }


    operator fun contains(employee: Employee) = employees.contains(employee)


    override fun iterator() = employees.iterator()
}


data class Employee(val company: Company, val name: String, var salary: Int) : Comparable<Employee> {

    //++
    operator fun inc(): Employee {
        salary += 5000
        println("$name got a raise to $$salary")
        return this
    }

    //+=
    operator fun plusAssign(increaseSalary: Int) {
        salary += increaseSalary
        println("$name got a raise to $$salary")
    }

    //-=
    operator fun minusAssign(decreaseSalary: Int) {
        salary -= decreaseSalary
        println("$name's salary decreased to $$salary")
    }


    override operator fun compareTo(other: Employee): Int {
        println("compareTo Employee")
        return when (other) {
            this -> 0
            else -> name.compareTo(other.name)
        }
    }

    operator fun rangeTo(other: Employee): List<Employee> {
        val currentIndex = company.allEmployees.indexOf(this)
        val otherIndex = company.allEmployees.indexOf(other)
        // start index cannot be larger or equal to the end index
        if (currentIndex >= otherIndex) {
            return emptyList()
        }
        // get all elements in a list from currentIndex to otherIndex
        return company.allEmployees.slice(currentIndex..otherIndex)
    }

}

//Delegated properties as conventions
class NameDelegate {
    operator fun getValue(thisRef: Any?, property: KProperty<*>): String {
        println("getValue NameDelegate")
        return ""
    }

    operator fun setValue(
        thisRef: Any?, property: KProperty<*>, value:
        String
    ) {
        println("setValue NameDelegate")
        // set received value
    }
}