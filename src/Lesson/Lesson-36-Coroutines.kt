package Lesson

import kotlin.concurrent.thread


fun main(args: Array<String>) {


    println("Пример работы потоков в котлине")
    thread(start = true, name = "another thread") {
        (0..10).forEach {
            println("Message #$it from the ${Thread.currentThread().name}")
        }
    }


    (0..10).forEach {
        println("Message #$it from the ${Thread.currentThread().name}")
    }



}

