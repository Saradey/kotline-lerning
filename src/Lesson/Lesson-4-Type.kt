package Lesson

fun main(args: Array<String>) {

    var integer = 100
    var decimal = 12.5
    //integer = decimal

    integer = decimal.toInt()   //конвертация

    val characterA: Char = 'a'
    //val characterA2: Char = 'ad' error

    println(characterA)

    val myString: String = "Hello world";
    println(myString)

    val myString2: String = "213" + "123"
    println(myString2)
    val myString3 = "13"+"123"

    println(myString3)

    val a: Short = 12
    val b: Byte = 120
    val c: Int = -100000
    val answer = a + b + c // Answer will be an Int

    val anyNumber: Any = 42
    val anyString: Any = "42"

    val yes: Boolean = true
    val no: Boolean = false

    val doesOneEqualTwo = (1 == 2)

    val doesOneNotEqualTwo = (1 != 2)

    val alsoTrue = !(1 == 2)

    val and = true && true

    val or = true || false


    println()
    println()
    println("Byte:")
    val bytes = 0b11010010_01101001_10010100_10010010

    println(bytes)

}