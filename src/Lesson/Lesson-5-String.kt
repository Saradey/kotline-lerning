package Lesson

fun main(args: Array<String>) {

    var mystring1: String = "Jon"

    //templates
    var mysrtring2: String = "Hello $mystring1"

    println("Удобный вывод string:")
    println(mysrtring2)


    println()
    println()
    println("Удобный вывод string c числами:")
    var oneThird = 1.0 / 3.0
    var oneThirdLongString = "One third is $oneThird as a decimal."

    println(oneThirdLongString)

    val oneThirdLongString2 = "One third is ${1.0 / 3.0} as a decimal."
    println(oneThirdLongString2)


    println()
    println()
    println("Вывож стринг с имспользованием trimMargin")
    val bigString = """|You can have a string
  |that contains multiple
  |lines
  |by
  |doing this.
  """.trimMargin() //Multi-line strings
    println(bigString)
    println()
    println()


    val mystr = "cat"
    val mystr2 = "dog"
    println("Сравнение строк")
    println(mystr == mystr2)
    println(mystr < mystr2)
    val mystr3 = "cat2"
    val mystr4 = "do2 "
    println(mystr3 < mystr4)

    val mystr5 = "g"
    val mystr6 = "e"
    println(mystr5 > mystr6)


    var aLotOfAs = ""
    while (aLotOfAs.length < 10) {
        aLotOfAs += "a"
    }

    println(mystr.length)


    println()
    println()
    //let очень полезен при работе с объектами, которые могут принимать
    // значение null. Вместо того чтобы создавать бесконечные цепочки выражений
    // if-else, можно просто скомбинировать оператор ? (называется «оператор
    // безопасного вызова») с let: в результате вы получите лямбду, у которой
    // аргумент it является не-nullable-версией исходного объекта.
    println("let:")
    val players = mutableListOf("Alice", "Bob", "Cindy", "Dan")
    val minPlayer = players.min()
    minPlayer.let {
        println("$minPlayer will start") // > Alice will start
    }


}