package Lesson

fun main(args: Array<String>) {

    val coordinate: Pair<Int, Int> = Pair(2, 3)

    val coordinatesInferred = Pair(2, 3)

    val coordinatesWithTo = 2 to 3

    val coordinatesMixed = Pair(2.1, 3)
    // Inferred to be of type Pair<Double, Int>

    val x = coordinate.first
    val y = coordinate.second

    println("Вывод Pair:")
    println(coordinate)

    println()
    println()
    val (x2, y2) = coordinate

    println("Вывод x y Pair:")
    println(x2)
    println(y2)

    val coordinates3D = Triple(2, 3, 1)
    val (x3, y3, z3) = coordinates3D

    println()
    println()
    println("Вывод Triple:")
    println(coordinates3D)

    val coordinates4D = Triple(2, 3, 1)
    val x4 = coordinates3D.first
    val y4 = coordinates3D.second
    val z4 = coordinates3D.third

    val (x5, y5, _) = coordinates4D
    println()
    println()
    println("Вывод x y z Triple:")
    println(x5)
    println(y5)


}