package Lesson

fun main(args: Array<String>) {

    println("\n\nСработала обычная функция:")
    add()

    println("\n\nСработала Unit функция:")
    add2()

    //Lesson.doNothingForever()


    println("\n\nФункция принимает значения:")
    printMultipleOfFive(10)

    println("\n\nФункция принимает два значения:")
    printMultipleOf(5, 5)

    println("\n\nФункция принимает Any:")
    myTestFunction(5)
    myTestFunction("SSSSS")

    println("\n\nФункция возвращает значения:")
    println("${myReturnValue()} return value")


    println("\n\nФункция возвращает Pair:")
    val (product, quotient) = multiplyAndDivide(4, 2)

    println("$product $quotient value returned")

    println("\n\nФункция в одну строчку:")
    println("${multiplyInferred(5, 5)} inline function")

    println("\n\nФункция в одну строчку:")
    println("${multiplyInferred2()} my inline function")

    println("\n\nФункция в одну строчку с другими функциями:")
    println("${multiplyInferred3(5)} my inline function 2")


    //Functions as variables

    println("\n\nФункция как переменная:")
    var myfunction = ::add3

    println(myfunction(2, 3))

    println("\n\nФункция принимает в себя другую функцию:")
    printResult(::add3, 4, 2)

    printResult2(myfunction)

    printResult3(myfunction, 3, 3)

    println("\n\nРекурсия:")
    recursive(3)
}


fun add() { //void
    println(5 + 5)
}

fun add2(): Unit {    //void
    println(5 + 5)
}

fun doNothingForever(): Nothing {   //TODO до конца разобраться с Nothing
    while (true) {
    }
}

fun printMultipleOfFive(value: Int) {
    println("$value * 5 = ${value * 5}")
}

fun printMultipleOf(multiplier: Int, andValue: Int) {
    println("$multiplier * $andValue = ${multiplier * andValue}")
}

fun myTestFunction(number: Any) {
    println("$number this Any")
}

fun myReturnValue(): Int {
    return 0
}

fun multiplyAndDivide(number: Int, factor: Int): Pair<Int, Int> {
    return Pair(number * factor, number / factor)
}


fun multiplyInferred(number: Int, multiplier: Int) = number * multiplier

fun testFunToInline() = 5

fun testFunToInline2() = 5

fun multiplyInferred2() = testFunToInline() * testFunToInline2()


fun testFunToInline3(number: Int): Int {
    return number * number
}

fun testFunToInline4(number: Int): Int {
    return number * number
}

fun multiplyInferred3(number: Int) = testFunToInline3(number) * testFunToInline4(number)


fun incrementAndPrint(value: Int) {
    //value += 1    //error
    print(value)
}

fun incrementAndPrint2(value: Int): Int {
    val newValue = value + 1
    println(newValue)
    return newValue
}

//перегрузка
fun getValue(value: Int): Int {
    return value + 1
}

fun getValue(value: String): String {
    return "The value is $value"
}

fun add3(a: Int, b: Int): Int {
    return a + b
}

//Передаем функции, другую функцию
fun printResult(function: (Int, Int) -> Int, a: Int, b: Int) {
    val result = function(a, b)
    print(result)
}

fun printResult2(function: (Int, Int) -> Int) {
    val result = function(5, 5)
    println("$result printResult2")
}


fun printResult3(function: (Int, Int) -> Int, a: Int, b: Int) {
    val result = function(a, b)
    println("$result printResult3")
}


//мы как бы говорим компилятору, что эта функция никогда не закончится
fun infiniteLoop(): Nothing {
    while (true) {
    }
}


fun recursive(number: Int) : Int{
    var teamp = number
    teamp--
    println("recursive $teamp")

    if(teamp != 0)
        return recursive(teamp)
    else return 0
}


