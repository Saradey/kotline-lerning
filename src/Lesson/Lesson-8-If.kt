package Lesson

fun main(args: Array<String>) {

    println("\n\nУсловия:")
    if(5 > 4){
        println("Test")
    }

    val animal = "Fox"
    if (animal == "Cat" || animal == "Dog") {
        println("Animal is a house pet.")
    } else {
        println("Animal is not a house pet.")
    }


    val hourOfDay = 12
    val timeOfDay = if (hourOfDay < 6) {
        "Early morning"
    } else if (hourOfDay < 12) {
        "Morning"
    } else if (hourOfDay < 17) {
        "Afternoon"
    } else if (hourOfDay < 20) {
        "Evening"
    } else if (hourOfDay < 24) {
        "Late evening"
    } else {
        "INVALID HOUR!"
    }
    println(timeOfDay)

    if (1 < 2 && "Matt Galloway" == "Matt Galloway") {
        println("AND")
    }




}