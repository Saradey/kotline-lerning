package Lesson

fun main(args: Array<String>) {


    var sum = 1
    while (sum < 1000) {
        sum = sum + (sum + 1)
    }


    sum = 1
    do {
        sum = sum + (sum + 1)
    } while (sum < 1000)


    sum = 1
    while (true) {
        sum = sum + (sum + 1)
        if (sum >= 1000) {
            break
        }
    }


    println("\n\nЦикл for и ranges:")
    val count = 10
    var sum1 = 0
    for (i in 1..count) {
        sum1 += i
        print(sum1)
        print(" ")
    }


    println("\n\nЦикл for и ranges:")
    val closedRange = 0..5
    for (i in closedRange) {
        print(i)
        print(" ")
    }


    println("\n\nФункция repeat как цикл:")
    repeat(10) {
        print(it)
        print(" ")
    }

    println("\n\nЦикл for и ranges с шагом 2:")
    for (i in 1..count step 2) {    //ходим через шаг 2
        print(i)
        print(" ")
    }

    println("\n\nЦикл for и ranges с шагом 2 идем вниз:")
    for (i in count downTo 1 step 2) {
        print(i)
        print(" ")
    }

    val decreasingRange = 5 downTo 0
    for (i in decreasingRange step 2) {
        print(i)
        print(" ")
    }

    println("\n\nЦикл for и until:")
    for (row in 0 until 8) {
        if (row % 2 == 0) {
            continue
        }
        print(row)
        print(" ")
    }

    println("\n\nЦикл for в цикле for:")
    for (row in 0 until 8) {
        for (column in 0 until 8) {
            print("R:$row C:$column ")
        }
        println()
    }


    println("\n\nЦикл for в цикле for c использованием условий и анотаций")
    myrowLoop@ for (row in 0 until 8) {
        println()
        columnLoop@ for (column in 0 until 8) {
            if (row == column) {
                continue@myrowLoop
            }
            print("R:$row C:$column ")
        }
    }

    println("\n\nЦикл for идем вниз с шагом 2")
    for (row in 10 downTo 0 step 2)
        print("$row ")



    println("\n\nЦикл for обходим mutableList")
    var players = mutableListOf("First", "Second", "Third")
    for (player in players) {
        println(player)
    }

    println("\n\nЦикл for обходим mutableList получаем pair в котором индекс и значение")
    for ((index, player) in players.withIndex()) {
        println("${index + 1}.$player")
    }

    println("\n\nЦикл for в функции")
    println(sumOfElements(listOf(1, 2, 3, 5, 6)))



    val bobData = mutableMapOf(
        "name" to "Bob",
        "profession" to "CardPlayer",
        "country" to "USA")

    println("\n\nЦикл for обходим mutableMap:")
    for ((player, score) in bobData) {
        println ("$player - $score")
    }


    println("\n\nЦикл for обходим mutableMap keys:")
    for (player in bobData.keys) {
        print("$player, ") // no newline
    }
    println()
}


fun sumOfElements(list: List<Int>): Int {
    var sum = 0
    for (number in list) {
        sum += number
    }
    return sum
}