package Practice

fun main(args: Array<String>) {

    //стр 161
    //Task 1
    println("\nTask 1")
    val nameList = mutableListOf(
        "Jon ", "Sarad ", "Doom ",
        "Jerahs ", "Andrey "
    )

    val concatenation = nameList.fold("") { a, b ->
        a + b
    }

    println(concatenation)
    println()



    //Task 2
    println("\nTask 2")
    val concatenation2 = nameList.filter {
        it.length <= 5
    }.fold("") { a, b ->
        a + b
    }
    println(concatenation2)



    //Task 3
    println("\nTask 3")
    val namesAndAges = mapOf(
        "Evgeny Goncharov" to 24,
        "Artem Sergeevich" to 31,
        "Test Test" to 17,
        "Test2 Test2" to 12
    )



    var mladhe18 = namesAndAges.filter {
        it.value < 18
    }
    println(mladhe18.toString())



    //Task 4
    println("\nTask 4")
    var arname = ArrayList<String>(namesAndAges.filter {
        it.value > 18
    }.map {
        it.key
    })



    println(arname.toString())
}