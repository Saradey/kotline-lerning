@file:JvmName("UserExtensions")

package dop

import Lesson.Address3
import myjava.Lesson30User

//добавили java классу переменную
val Lesson30User.fullName: String
    get() = firstName + " " + lastName


//добавили классу java функцию
fun Lesson30User.addressOfType(): Address3? {
    return addresses.firstOrNull { it != null }
}

//добавили классу java функцию
fun Lesson30User.addOrUpdateAddress(address: Address3) {
    val existingOfType = addressOfType()
    if (existingOfType != null) {
        addresses.remove(existingOfType)
    }

    addresses.add(address)
}


fun main(args: Array<String>) {

    var user = Lesson30User()

    println("\n\nПример взаимодействия java с котлином")
    user.addOrUpdateAddress(Address3())
    println("User info after adding address:\n$user")

    val shippingAddress = Address3()
    user.addOrUpdateAddress(shippingAddress)


    val a1 = Address3()
    val a2 = Address3()
    val a3 = Address3()
    var lesson30user = Lesson30User()
    lesson30user.addOrUpdateAddress(a1)
    lesson30user.addOrUpdateAddress(a2)
    lesson30user.addOrUpdateAddress(a3)

    println("\n\n вызываю функцию java allAddresses")
    println(lesson30user.allAddresses())


    println("\n\nJava nullability annotations:")
    //по идеи должны быть ошибки
    val anotherUser = Lesson30User()
    anotherUser.addresses = null
    println(anotherUser.addresses)



}


//Free functions
fun labelFor(user: Lesson30User, type: Address3): String {
   return "function labelFor"
}