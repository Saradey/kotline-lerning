package myjava;


import Lesson.Address3;
import com.sun.istack.internal.NotNull;
import dop.UserExtensions;

import java.util.ArrayList;
import java.util.List;

public class Lesson30User {

    public String firstName = "Tomas";
    public String  lastName = "Denve";

    private List<Address3> addresses = new ArrayList<>();


    @Override
    public String toString() {
        return UserExtensions.getFullName(this);
    }


    @NotNull
    public List<Address3> getAddresses() {
        return addresses;
    }

    public void setAddresses(List<Address3> addresses) {
        this.addresses = addresses;
    }

    //free function from kotline
    public String allAddresses() {
        StringBuilder builder = new StringBuilder();
        for (Address3 address : addresses) {
            builder.append(address.getFullName() + " address:\n");
            builder.append(UserExtensions.labelFor(this,
                    address));
        }
        return builder.toString();
    }


}
